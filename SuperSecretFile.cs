bool isLooping = true;
int dataEntryCount = 0;
string firstName = null;
string lastName = null;

while (isLooping)
{
    if (firstName != null)
    {
        Console.WriteLine();
        Console.WriteLine("(the previous person (# " + dataEntryCount + ") was " + firstName + " " + lastName + ")");
    }
    Console.WriteLine("First Name: ");
    firstName = Console.ReadLine();
    Console.WriteLine("Last Name: ");
    lastName = Console.ReadLine();
    Console.WriteLine("Political Party Affiliation: ");
    string party = Console.ReadLine();
    Console.WriteLine("Voting in next Presidental Election?");
    string voting = Console.ReadLine();

    Console.WriteLine();

    Console.WriteLine("========");
    Console.WriteLine(firstName + " " + lastName);
    Console.WriteLine(party + "/" + voting);
    Console.WriteLine("========");

    dataEntryCount++;
}
