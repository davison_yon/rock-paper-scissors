// Name: Sussy Mc. Sus Sus
// Title: Rock, Paper, Scissors
// Date: 9/16/22

int playerScore = 0;
int computerScore = 0;
int playerSelection = 0;


bool gameOver = false;


while (!gameOver)
{
    Console.WriteLine("Rock, Paper, or Scissors?");
    string selection = Console.ReadLine();

    if (selection == "Rock")
    {
        Rock();
    }
    else if (selection == "Paper")
    {
        Paper();
    }
    else if (selection == "Scissors")
    {
        Scissors();
    }
    else
    {

    }

    void Rock()
    {
        playerSelection = 1;
        int computer = ComputersTurn();

        if (computer == 1)
        {
            Console.WriteLine("Rock vs Rock, it's a tie!");
        }
        else if (computer == 2)
        {
            Console.WriteLine("Computer picked paper. Paper beats Rock");
            computerScore++;
        }
        else if (computer == 3)
        {
            Console.WriteLine("Computer picked scissors. Rock beats scissors!");
            playerScore++;
        }
    }

    void Paper()
    {
        playerSelection = 2;
        int computer = ComputersTurn();

        if (computer == 1)
        {
            Console.WriteLine("Computer picked Rock. Paper beats rock!");
            playerScore++;
        }
        else if (computer == 2)
        {
            Console.WriteLine("Computer picked Paper. Tie!");
        }
        else if (computer == 3)
        {
            Console.WriteLine("Computer picked Scissors. Scissors beats paper!");
            computerScore++;
        }
    }

    void Scissors()
    {
        playerSelection = 3;
        int computer = ComputersTurn();

        if (computer == 1)
        {
            Console.WriteLine("Computer picked Rock. Rock beats scissors!");
            computerScore++;
        }
        else if (computer == 2)
        {
            Console.WriteLine("Computer picked Paper. Scissors beats paper!");
            playerScore++;
        }
        else if (computer == 3)
        {
            Console.WriteLine("Computer picked Scissors. Tie!");
        }
    }

    int ComputersTurn()
    {
        Random random = new Random();
        int computersSelection = random.Next(1, 3);
        return computersSelection;
    }

}
